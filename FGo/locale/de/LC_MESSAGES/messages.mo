��    6      �  I   |      �     �     �     �     �     �     �  J   �     1     7     D     L     T     Z     m     r     w     �     �     �     �     �     �     �     �     �  j   �     Y     _     k     p     �     �     �     �  
   �     �     �     �     �     �     �          
  #   '  *   K     v     �     �     �     �     �  !   �       �  "     �	     �	  
   �	  	   �	     �	     �	  ]   
  
   e
     p
  	   
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
            
          �   =     �     �     �     �     �  
     	             2     F     ^     g     v     }     �     �  $   �  !   �  (   �     $     5  	   M  8   W  /   �     �      �                  ,   %   $                        	   (   /         !   '   0                +                      4                                  #             "                      &   -      2       5      3   
   6       .      )              *             1    About Airport data source: Airport: Cancel Carrier: Change language: Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Error Fetching report... File Find FlightGear is running... FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Search Select Scenario Selected Scenarios: Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Translation: Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2010-08-04 21:15+CEST
PO-Revision-Date: 2010-05-24 00:32+0200
Last-Translator: chris_blues <chris@musicchris.de>
Language-Team: LANGUAGE <LL@li.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.5.2
Generated-By: pygettext.py 1.5
 Über Flughafen-Datenquelle: Flughafen: Abbrechen Träger: Sprache ändern: Hier klicken, um METAR Bericht für den
gewählten (oder nahesten) Flughafen herunterzuladen. Schließen Config Dateien Dekodiert Standard Fehler Hole Wetterbericht... Datei Finden Flightgear läuft... FlightGear Einstellungen Hilfe Lizenz Laden Verschiedenes Keine OK Parkplatz: Pfad zur ausführbaren Datei: Bitte stellen sie sicher, daß die Pfade FG_BIN und FG_ROOT
im "Eigenschaften"-Fenster auf die richtigen Verzeichnisse verweisen. Port: Eigenschaften Beenden Flughafen-Datenbank neu laden Zurücksetzen FG starten Rollbahn: Speichern und beenden Speichern unter ... Einstellungen speichern Szenerie Szenerie Pfad: Suchen Szenario wählen Ausgewählte Szenarios: Einstellungen Nur installierte Flughäfen anzeigen Starte %s mit folgenden Optionen: Starte TerraSync mit folgendem Kommando: Stoppe TerraSync TerraSync Einstellungen Werkzeuge deutsche Übersetzung:
chris_blues <chris@musicchris.de> Die Daten können nicht heruntergeladen werden. Kann FlightGear nicht starten! Liste der Flughäfen auffrischen Arbeitsverzeichnis (optional): 