��    5      �  G   l      �     �     �     �     �     �     �  J   �               ,     4     <     B     U     Z     _     x     �     �     �     �     �     �     �     �  j   �     A     G     S     X     q     w     ~     �  
   �     �     �     �     �     �     �     �     �  #     *   3     ^     q     �     �     �  !   �     �  �  �     �	     �	     �	     �	     �	     �	  J   �	     &
     ,
     9
     A
     I
     O
     b
     g
     l
     �
     �
     �
     �
     �
     �
     �
     �
     �
  j   �
     N     T     `     e     ~     �     �     �  
   �     �     �     �     �     �     �     �     �  #     *   @     k     ~     �     �     �  !   �     �     0                /      4   2   (          '                                                    *             !              3             1      -          +   $                 5   )   "          ,   &                .   %   #      	             
    About Airport data source: Airport: Cancel Carrier: Change language: Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Error Fetching report... File Find FlightGear is running... FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Search Select Scenario Selected Scenarios: Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2010-08-04 21:15+CEST
PO-Revision-Date: 2010-07-14 20:00+0200
Last-Translator: Robert 'erobo' Leda <erobo@wp.pl>
Language-Team: LANGUAGE <LL@li.org>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.5.2
Generated-By: pygettext.py 1.5
 About Airport data source: Airport: Cancel Carrier: Change language: Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Error Fetching report... File Find FlightGear is running... FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Search Select Scenario Selected Scenarios: Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): 