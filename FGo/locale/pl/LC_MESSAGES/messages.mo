��    5      �  G   l      �     �     �     �     �     �     �  J   �               ,     4     <     B     U     Z     _     x     �     �     �     �     �     �     �     �  j   �     A     G     S     X     q     w     ~     �  
   �     �     �     �     �     �     �     �     �  #     *   3     ^     q     �     �     �  !   �     �  �  �     �	     �	  	   �	     
  	   
     
  Q   %
     w
     
     �
  	   �
     �
     �
     �
     �
  !   �
     �
                    #     +     2     5  !   >  b   `     �     �     �     �     �  
   �     
               .     @     I     `     h     {  
   �  #   �  (   �  -   �          ,  
   A     L  !   g  (   �     �     0                /      4   2   (          '                                                    *             !              3             1      -          +   $                 5   )   "          ,   &                .   %   #      	             
    About Airport data source: Airport: Cancel Carrier: Change language: Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Error Fetching report... File Find FlightGear is running... FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Search Select Scenario Selected Scenarios: Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2010-08-04 21:15+CEST
PO-Revision-Date: 2010-08-05 01:59+0200
Last-Translator: Robert 'erobo' Leda <erobo@wp.pl>
Language-Team: LANGUAGE <LL@li.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Virtaal 0.5.2
Generated-By: pygettext.py 1.5
 O programie Źródło danych o lotniskach: Lotnisko: Anuluj Lot-wiec: Zmień język: Kliknij tu, aby pobrać raport pogodowy
z wybranego (lub najbliższego) lotniska. Zamknij Pliki Konfiguracyjne Rozkodowany Domyślny Błąd Pobieranie raportu... Plik Znajdź FlightGear został uruchomiony... Ustawienia FlightGear Pomoc Licencja Wczytaj Różne Żaden OK Parking: Ścieżka do pliku wykonywalnego: Upewnij się, że ścieżki: FG_BIN i FG_ROOT
w oknie "Preferencje" wskazują właściwe katalogi. Port: Preferencje Zakończ Przebuduj Bazę Lotnisk Zresetuj Uruchom FG Pas: Zapisz i Wyjdź Zapisz jako... Zapisz ustawienia Sceneria Ścieżka do scenerii: Znajdź Wybierz Scenariusz Wybrano Scenariuszy: Ustawienia Pokaż tylko zainstalowane lotniska Uruchamiam %s z następującymi opcjami: Uruchamiam TerraSync następującą komendą: Zatrzymuję TerraSync Ustawienia TerraSync Narzędzia Nie można pobrać danych. Nie można uruchomić symulatora! Odśwież listę zainstalowanych lotnisk Katalog roboczy (opcjonalnie): 