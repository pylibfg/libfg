��    5      �  G   l      �     �     �     �     �     �     �  J   �               '     /     5     H     M     R     k          �     �     �     �     �     �     �  j   �     4     :     F     K     d     j     q     v  
   �     �     �     �     �     �     �     �     �  #     *   &     Q     d     w     }     �     �  !   �     �    �  	   }	     �	     �	     �	     �	     �	  `   �	     ;
     B
     P
     \
     b
     x
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
  x        �     �     �  (   �  	   �     �     �     �     �       	        "     9     B     S     m     u  *   �  1   �     �               %  '   <      d  *   �  !   �     /                .          2   '          &                                     0              )                            3             4      ,          *   #             1   5   (   !          +   %                -   $   "      	          
       About Airport data source: Airport: Cancel Carrier: Change language: Click here to download the METAR report
for selected (or nearest) airport. Close Decoded Default Error Fetching report... File Find FlightGear is running... FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Search Select Scenario Selected Scenarios: Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Translation: Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2010-08-04 21:15+CEST
PO-Revision-Date: 2010-05-23 23:35+0200
Last-Translator: Canseco <machaquiro@yahoo.es>
Language-Team:
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.5.2
Generated-By: pygettext.py 1.5
 Acerca de Fuente de datos de aeropuertos: Aeropuerto: Cancelar Portaaviones: Cambiar Idioma: Haz click aquí para descargar el reporte METAR
para el aeropuerto seleccionado (o mas cercano). Cerrar Descodificado Por Defecto Error Obteniendo reporte... Archivo Buscar FlightGear esta ejecutandose... Ajustes de FlightGear Ayuda Licencia Cargar Miscelaneo Ninguno OK Parking: Ruta al archivo ejecutable: Por favor, asegurate que las rutas: FG_BIN and FG_ROOT
en el submenu "Preferencias" apuntan a los directorios correctos. Puerto: Preferencias Salir Reconstruir Base de Datos de Aeropuertos Reiniciar Ejecutar FG Pista: Guardar y Salir Guardar como... Guardar ajustes Escenario Ruta a los Escenarios: Busqueda Elegir Escenario Escenarios Seleccionados: Ajustes Mostrar aeropuertos instalados Ejecutando %s con las siguientes opciones: Ejecutando TerraSync con los siguientes comandos: Parando TerraSync Ajustes de TerraSync Herramientas Traducido por:
Canseco No ha sido posible descargar los datos. No se puede ejecutar FlightGear! Actualizar lista de aeropuertos instalados Directorio de trabajo (opcional): 