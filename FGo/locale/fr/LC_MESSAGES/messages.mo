��    5      �  G   l      �     �     �     �     �     �     �  J   �               ,     4     <     B     U     Z     _     x     �     �     �     �     �     �     �     �  j   �     A     G     S     X     q     w     ~     �  
   �     �     �     �     �     �     �     �     �  #     *   3     ^     q     �     �     �  !   �     �  �  �     �	     �	  
   �	     �	     �	     �	  Z   �	     V
     ]
     u
     ~
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
               
  %     X   9     �     �     �  .   �     �  	   �     �     �          "     <     D  	   ]     g     �  	   �  -   �  '   �  .   �     +     =     Q  )   X  $   �  2   �  #   �     0                /      4   2   (          '                                                    *             !              3             1      -          +   $                 5   )   "          ,   &                .   %   #      	             
    About Airport data source: Airport: Cancel Carrier: Change language: Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Error Fetching report... File Find FlightGear is running... FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Search Select Scenario Selected Scenarios: Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2010-08-04 21:15+CEST
PO-Revision-Date: 2010-08-08 17:41+0100
Last-Translator: Olivier Faivre <olivier.faivre25@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Virtaal 0.5.2
Generated-By: pygettext.py 1.5
 A propos Source des données aéroport: Aéroport: Annuler Porte-avions: Changer la langue: Cliquer ici pour télécharger le METAR
pour l'aéroport sélectionné (ou le plus proche) Fermer Configurer les fichiers Décodé Défaut Erreur Récupérer le rapport... Fichier Trouver FlightGear est lancé... Réglages FlightGear Aide License Charger Divers Aucun Ok Parking: Chemin vers les fichiers exécutable: Vérifiez que le chemin : FG_BIN and FG_ROOT
in "Preferences"  pointe sur le bon dossier Port: Préférences Quitter Reconstruire la base de donnée des aéroports Redémarrer Lancer FG Piste: Enregistrer et quitter Enregistrer sous Enregistrer les réglages Scènes Chemin vers les scènes: Recherche Séléctionner un scénario Scénario séléctionné Réglages Montrer  uniquement les aéroports installés Démarrer %s avec les options suivantes Démarrer TerraSync avec la commande suivante: Stopper TerraSync Réglages TerraSync Outils Impossible de télécharger les données. Impossible de démarrer FlightGear ! Mise à jour de la liste des Aéroports installés Répertoire de travail (optionnel): 