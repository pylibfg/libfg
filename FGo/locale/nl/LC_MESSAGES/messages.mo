��    ,      |  ;   �      �     �     �     �     �     �     �                    "     '     ,     @     E     M     R     `     e     h     q  j   �     �     �               %     +     2     7  
   C     N     \     d     r     y     �     �  #   �  *   �     �               5  h  S     �     �  
   �  	   �     �  
   �       	                  #     ,     D     I     R     Y     `     e     h     w  g   �     �  
   	  	   	     	     9	  
   ?	  
   J	     U	     g	     v	  	   �	     �	     �	     �	     �	     �	  '   �	  0   
     <
     N
     e
     �
     ,             (   *       !   )         &                             +   "                          %          #           '                                  
                      $      	               About Airport data source: Airport: Cancel Carrier: Change language: Close Default Error File Find FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Search Select Scenario Selected Scenarios: Settings Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Unable to run FlightGear! Working directory (optional): Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2010-05-23 23:22+CEST
PO-Revision-Date: 2010-05-23 23:35+0200
Last-Translator: snipey
Language-Team:
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.5.2
Generated-By: pygettext.py 1.5
 Over Vliegveld gegevens: Vliegveld: Annuleren Vliegdekschip: Kies taal: Sluiten Standaard Fout Bestand Bladeren FlightGear instellingen Help Licentie Openen Overig Geen OK Parkeerplaats: Pad naar uitvoerbaar bestand: Controleer AUB of de paden: FG_BIN en FG_ROOT
in het "Preferences" scherm naar de juiste mappen wijzen. Poort: Voorkeuren Afsluiten Maak Vliegveld Database Opnieuw Reset FG Starten Startbaan: Opslaan & Sluiten Opslaan als... Instellingen opslaan Landschap Scenery pad: Zoeken Selecteer Scenario Geselecteerde Scenario's: Instellingen %s wordt gestart met de volgende opties TerraSync wordt gestart met volgende commando's: TerraSync Stoppen TerraSync instellingen Kan FlightGear niet starten Actieve map (optioneel): 