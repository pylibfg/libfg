FGo! is using gettext library to localize its interface. Translations (with
some exceptions) are stored in "src/locale" directory. You are very welcome
to update/improve existing one, or add translation to new language if you wish.
Even smallest contribution will be appreciated.


Here are some helpful (I hope) tips:



IF YOU ARE FAMILIAR WITH GETTEXT AND WOULD LIKE TO CONTRIBUTE A NEW, OR
MODIFY AN EXISTING TRANSLATION:

You should have no trouble with the task. Translating FGo! is no different than
translating any other application. Except maybe some additional files, see SOME
ADDITIONAL TEXT TO TRANSLATE section for more information. When translation is
ready, please send me "messages.po", and/or other files at mail address
provided at the end of this document. I'll be happy to include you translation
in next release of FGo!



IF YOU LIKE TO MODIFY EXISTING TRANSLATION BUT NEVER TRANSLATED AN APPLICATION
BEFORE:

Don't worry, it is quite simple. All you need to do is to edit a "messages.po"
file located in "src/locale/LANGUAGE_CODE/LC_MESSAGES" directory. You can do
this by using any text editor. But using dedicated software (e.g. Poedit,
Virtaal, or Emacs) is more convenient and less error-prone way to edit
a PO file. Translation process itself is straightforward and you should
have no trouble with the task at all. One thing to remember however, is
to include in your translation all "%s" formatting characters from original
text. It is a placeholder for some values to be displayed onscreen, e.g. it is
used to display path to executable file for FlightGear in console message,
and its absence can lead to crash of the application.
When you're happy with the changes you made to PO file, one last thing to do,
is to generate new "messages.mo" file, which is actually used by the program
to show translated messages. To do this, open console, navigate to folder
containing "messages.po" file and run this command:

msgfmt messages.po

Now you can inspect if changes you have made are properly included in FGo!
If you're happy with the result, please send me "messages.po" file at mail
address provided at the end of this document. I'll be happy to include you
translation in next release of FGo!

See section SOME ADDITIONAL TEXT TO TRANSLATE for some more information.



IF YOU LIKE TO ADD NEW TRANSLATION BUT NEVER TRANSLATED AN APPLICATION BEFORE:

First you need to create a new folder in "src/locale" directory. This
new folder should have the same name as language code for your language.
Some of most common examples of language codes can be found here:
http://www.gnu.org/software/hello/manual/gettext/Usual-Language-Codes.html
Next, navigate to this newly created directory and create "LC_MESSAGES"
folder. Rest of the translation process is the same as described in
section IF YOU LIKE TO MODIFY EXISTING TRANSLATION BUT NEVER TRANSLATED
AN APPLICATION BEFORE. With one exception: you should edit "messages.pot" file
in "src/locale" directory, and then save it as "messages.po" into 
"src/locale/LANGUAGE_CODE/LC_MESSAGES" folder.

After you have created "messages.po" file you may open it with simple text
editor and edit some comments:

* At first line, replace word LANGUAGE with actual name of your language.

* At fourth line, provide your personal data (if you wish of course) as you
  are first author of this translation.

See next section for some more information.



SOME ADDITIONAL TEXT TO TRANSLATE:

Additionally, you can also translate files in the "src/config/"
and "src/help/". Content from the first directory is shown in Command Line
Options Window (the text window at the bottom of the application) at first run
of FGo!, and content from the second directory is displayed in the help window.

If no file with your language code is present, open file "en", and save it at
the same name as the directory with your translation (in "src/locale")
is called.

Please take a note that no line in those files should have more than 79
characters. In case of the help file it is mandatory to stay within that
79 characters limit, while in case of the config it is only a general advice
and can be exceeded slightly if appropriate. In many text editors, you can
enable right margin line which can help you to not exceed this limit.

I understand that translating help file can be a major task. Due to its length,
and not the best style, not to mention my poor English proficiency, or fact,
that this file is subject to change in the future I actually encouraged you
to not translate this file literally if you only find it appropriate. In fact,
you may write your help from scratch if you wish. I encouraged you however to
include information that can be found in: MINIMUM REQUIREMENTS, INSTALLATION,
RUNNING, and CONFIGURATION sections, as I personally considering these chapters
as most important for new users.



TESTING YOUR TRANSLATION:

When your translation is ready, you can test if all text is properly displayed
in the application. Navigate to "~/.fgo" directory, and move out, or rename
temporarily file called "config". It will simulate a situation when FGo! is
run first time. After running the program, it should automatically pick up
translation based on you system settings. If no proper translation is found,
it should revert to default, English language. If you approach any troubles
at this point, please contact me at the address that can be found at the end
of this file. I will be happy to help you.



                                                     Thank you for your effort,
                                      Robert 'erobo' Leda  <erobo[AT]wp[DOT]pl>
