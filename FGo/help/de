FGo! 1.3.0
Ein einfacher GUI Starter für FlightGear.

-------------------------------------------------------------------------------

MINIMALE SYSTEMVORAUSSETZUNGEN

Betriebssystem GNU/Linux
FlightGear
Python 2.5.2 *
TKinter
Python Imaging Library (PIL) 1.1.6 (mit ImageTk Modul)
Tcl/Tk 8.4

* FGo! ist nur mit Python Serie 2.x kompatibel.

-------------------------------------------------------------------------------

INSTALLATION

FGo! braucht nicht installiert zu werden, allerdings muss Python (mit Tkinter-
und PIL-Modulen) installiert sein, bevor FGo! das erste Mal gestartet werden
kann. Alle Pakete sollten in den Repositories vorhanden sein.
In Debian-basierten Distributionen sollten folgende Pakete installiert werden:
python, python-tk, python-imaging, python-imaging-tk, tcl8.x und tk8.x.

Es sollte reichen einfach python-imaging-tk zum Installieren vorzumerken.
Die übrigen Abhängigkeiten sollten vom Paket-Manager automatisch mit
installiert werden.

-------------------------------------------------------------------------------

GESTARTET

FGo! bringt von Hause aus kein eigenes Terminal-Fenster mit, um Meldungen von
FlightGear anzuzeigen. Deshalb ist die beste Vorgehensweise FGo! aus der
Kommandozeile zu starten. Öffnen Sie ein Terminal, navigieren Sie zum Ordner,
in dem FGo! entpackt wurde, und starten Sie mit "python fgo" oder "./fgo".
Oder, falls das System die Möglichkeit bietet, klicken Sie einfach auf das
fgo-Symbol und wählen Sie: "Im Terminal ausführen". Bei beiden Möglichkeiten
müssen Sie sicherstellen, dass die fgo-Datei "ausführbar" ist.

Benutzt man FGo! erst seit Version 1.3.0, braucht man nicht mehr in das
Hauptverzeichnis zu wechseln um es zu starten. Man erstellt nur eine
symbolische Verknüpfung zur fgo-Datei, und legt sie irgendwo ab, oder man
startet es an jeder Position mit einem Kommando wie diesem:
python ~/FGo!_Haupt_Verzeichnis/fgo

-------------------------------------------------------------------------------

KONFIGURATION

Um FGo! benutzen zu können, muss man es erst einrichten. Öffnen Sie das
Einstellungsfenster (im Menü: Einstellungen => Eigenschaften) und füllen Sie
die leeren Felder aus.

FlightGear Einstellungen:

    Pfad zur ausführbaren Datei - Geben Sie den Ort an, wo sich die Datei
        "fgfs" oder, falls Sie Download_und_Kompilier-Skripte nutzen,
        "run_fgfs.sh" befindet.
    
    FG_ROOT - Pfad zu FlightGear's Hauptverzeichnis. z.Bsp.: 
        /usr/share/games/FlightGear/fgdata

    FG_SCENERY - Pfad zur Szenerie. Man kann mehrere Pfade angeben (mit
        Doppelpunkten getrennt). In der Reihenfolge von der höchsten zur
        niedrigsten Priorität.

    Arbeitsverzeichnis - optionaler Parameter, der das Arbeitsverzeichnis von
        FlightGear benennt. In diesem Verzeichnis werden Bildschirmfotos und
        Log-Dateien abgespeichert. Bleibt dieses Feld leer, wird das Home-
        Verzeichnis des Benutzers benutzt.

TerraSync Einstellungen:

    Pfad zur ausführbaren Datei - Geben Sie den Ort an, wo sich die Datei
        "terrasync" oder, falls Sie Download_und_Kompilier-Skripte nutzen,
        "run_terrasync.sh" befindet.
    Szenerie Pfad - Verzeichnis, wohin TerraSync die Szenerien herunterlädt.

    Port - eine Port-Addresse, die TerraSync benutzt.

Verschiedenes:

    Sprache ändern - Wählen Sie eine andere Sprache. Falls keine Sprache
        ausgewählt ist wird FGo! versuchen die System-Sprache zu erkennen.

    Flughafen-Datenquelle - Wählen Sie die Datenquelle, wo FGo! nach
        Informationen über Rollbahnen oder Parkpositionen suchen soll. Es gibt
        zwei Möglichkeiten:

        Standard - FGo! zeigt Rollbahnbezeichnungen aus der ~/.fgo/apt-
            Datei. Diese wird generiert aus der FG_ROOT/Airports/apt.dat.gz
            Datenbank. Parkplatznamen werden aus FG_ROOT/AI/Airports geholt.

        Szenerie - FGo! zeigt Rollbahnbezeichnungen aus der ~/.fgo/apt-
            Datei. Diese wird generiert aus der FG_ROOT/Airports/apt.dat.gz
            Datenbank. Parkplatznamen werden aus FG_SCENERY/Airports bzw. den
            angegeben Szenerie-Verzeichnissen geholt.

        Generell gesehen: Die Einstellung "Standard" veranlasst FGo! die
        selben Daten zu zeigen, die FlightGear normalerweise benutzt, wenn man
        die Startposition angibt, während die Option "Szenerie" FGo!
        veranlasst, direkt im angegebenen Szenerie-Verzeichnis nachzusehen. Im
        letzteren Fall könnten sie noch
        --prop:/sim/paths/use-custom-scenery-data=true im Kommandozeilen-
        fenster angeben, um FlightGear anzuzeigen, daß es die selben Daten
        benutzen soll.

        Sollten Sie sich unsicher sein, welche Option Sie benutzen sollten,
        ist es wahrscheinlich am Besten, wenn Sie die Standard-Option wählen.

    Flughafen-Datenbank neu laden - baut eine neue Flughafen-Datenbank aus
        FG_ROOT/Airports/apt.dat.gz. Das ist z.Bsp. sinnvoll wenn die Datei
        apt.dat.gz upgedatet wurde.

Das Ausfüllen der ersten drei Felder ist notwendig für die korrekte Ausführung
des Programmes - der Rest ist optional. Wenn Sie die Konfiguration
abgeschlossen haben klicken Sie auf "Einstellungen speichern". Nach wenigen
Augenblicken sollte im Hauptfenster eine Auflistung der verfügbaren Flugzeuge
und Flughäfen erscheinen.

Veränderungen in den Einstellungen werden nach "Einstellungen speichern"
sofort angewendet, allerdings benötigen Veränderungen in den  TerraSync-
Einstellungen und Spracheinstellungen einen Neustart von FGo!.

-------------------------------------------------------------------------------

HAUPTMENÜ EINTRÄGE

Datei:

    Laden - Lade eine vorhandene Konfigurations-Datei

    Speichern unter... - Speichere Einstellungen in eine Konfigurations-Datei

    Speichern und beenden - Speichere Einstellungen und beende die Anwendung

    Beenden - Beende die Anwendung ohne zu speichern

Einstellungen:

    Nur installierte Flughäfen anzeigen - Es werden nur die tatsächlich auf
        der Festplatte vorhandene Flughäfen angezeigt.

    Liste der Flughäfen auffrischen - Durchsuche die Festplatte nach
        installierter Szenerie und baue eine neue Flughafen-Liste. Das
        funktioniert nur wenn "Nur installierte Flughäfen anzeigen" angewählt
        ist.

    Eigenschaften - öffne das Eigenschaften-Fenster.

Werkzeuge:

    METAR - zeige METAR Report für den gewählten (oder nahesten) Flughafen.
        Dieser wird von http://weather.noaa.gov/ heruntergeladen.

Hilfe:

    Hilfe - Öffne dieses Hilfe-Fenster.

    Über - Öffne das "Über"-Fenster.

-------------------------------------------------------------------------------

KOMMANDOZEILEN OPTIONEN FENSTER

Im Text-Fenster am unteren Ende des FGo!-Fensters kann man Kommandozeilen-
Optionen angeben, die an FlightGear weitergereicht werden.

Es werden vorerst nur wenige Optionen angezeigt. Für mehr Informationen und
Beispiele bemühen Sie bitte die FlightGear-Dokumentation, prüfen Sie im
Terminal "fgfs --help --verbose" oder sehen Sie im Wiki nach:
http://wiki.flightgear.org/index.php/Command_Line_Options

-------------------------------------------------------------------------------

TIPS&TRICKS

* Wenn die Flughafen-Datenquelle auf "Szenerie" gesetzt ist, können
  Informationen über Parkpositionen solange nicht verfügbar sein, bis die
  entsprechende Szenerie installiert ist.

* Wenn *nur* Großbuchstaben in die Flughafen-Suchbox eigegeben werden, wird
  das Programm nur nach ICAO-Codes mit den entsprechenden Anfangsbuchstaben
  suchen. Zum Beispiel:
  Für die Großbuchstaben "ED" werden als Ergebnis (fast) nur deutsche
  Flughäfen angezeigt.

* Man kann auch von einem Flugzeugträger starten. Klicken Sie im mittleren
  Abschnitt auf den ICAO-Code des zuletzt gewählten Flughafens (direkt unter
  dem Flugzeugbild) und wählen Sie eins der verfügbaren Schiffe. Der ICAO-Code
  wird daraufhin durch den Namen des gewählten Flugzeugträgers ersetzt und
  blau hervorgehoben, um anzuzeigen dass Sie sich jetzt im "Flugzeugträger-
  Modus" befinden. Das entsprechende Szenario wird automatisch gewählt.
  Um wieder auf die Flughäfen zugreifen zu können müssen Sie nur auf den
  Flugzeugträgernamen klicken und "keine" auswählen.

* In der Liste "Szenario auswählen" können Sie sich, mit einem Rechtsklick auf
  ein Szenario, die Beschreibung dessen ansehen (falls verfügbar).

* Die Liste "Szenario auswählen" kann mit einem Klick aufs Mausrad bzw.
  mittlere Taste geschlossen werden.

* Die Fenstergröße wird gespeichert, wenn "Speichern und beenden" betätigt
  wird.

* Falls die Datei apt.dat.gz verändert wurde (z.Bsp. wenn eine neue Version
  von FlightGear installiert wurde), muß der Knopf "Flughafen-Datenbank neu
  laden" im Einstellungsfenster betätigt werden, um alle Änderungen in FGo!
  zu integrieren.

* Um den FGo! Starter besser von anderen Dateien unterscheiden zu können,
  können Sie das Standard-Symbol durch das Bild "icon.png" ersetzen. Diese
  Datei finden Sie im FGo!-Verzeichnis /fgo/src/pics.

-------------------------------------------------------------------------------

BEKANNTE FEHLER UND GRENZEN

* FGo! weiß nicht, ob TerraSync tatsächlich beendet wurde, bevor eine neue
  Instanz gestartet wird. Wenn das "TerraSync" Ankreuzfeld geleert und gleich
  wieder angekreuzt wird, kann die Meldung "error binding to port 5501" im
  Terminalfenster erscheinen. Dies zeigt an, dass eine alte Instanz von
  TerraSync noch nicht fertig ist mit dem Herunterladen der Daten. Warten Sie
  in diesem Fall bis das Herunterladen beendet wurde und kreuzen Sie das
  "TerraSync"-Feld danach wieder an.

* Sehr lange Parkplatznamen passen nicht in den Parkplatz-Knopf.

-------------------------------------------------------------------------------

                                 Vielen Dank, dass Sie diese Software benutzen,
                                      Robert 'erobo' Leda  <erobo[AT]wp[DOT]pl>
