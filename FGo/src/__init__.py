#!/usr/bin/env python
"""FGo! - a simple GUI launcher for FlightGear Flight Simulator."""

from os import chdir
from Tkinter import Tk

from config import Config
from gui import App


def run(working_dir):
    """Initialize application."""
    # Set current working dirrectory.
    chdir(working_dir)

    root = Tk()
    root.title('FGo!')
    # Initialize data object.
    data = Config()
    # Initialize main window.
    app = App(root, data)

    # Set window resolution.
    window_geometry = data.window_geometry.get()
    if window_geometry:
        root.geometry(window_geometry)

    # Override window close button, so TerraSync
    # can be stopped before closing the program.
    root.protocol("WM_DELETE_WINDOW", app.quit)

    root.mainloop()

if __name__ == '__main__':
    from sys import path

    WORKING_DIR = path[0]
    run(WORKING_DIR)
