#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from Tkinter import *
from ImageTk import PhotoImage
from ScrolledText import ScrolledText

import gettext

from libfg import *
from libts import *

import constants


class FGo:

    """Application's main window."""

    def __init__(self):
        """FGo's core function.
        calling this will start the whole program"""
        
        # 'fg' and 'ts' will get instances of the FlightGear and Terrasync class in libfg, or libts
        # and contain all the neccessary information for running both
        self.fg = None
        self.ts = None
        
        # 'lang_code' will contain the name of the locale in use
        self.lang_code = ''
        
        # init Tk as we need tcl-variables in the init-functions
        self.main_window = Tk()
        
        print('loading configuration...')
        # try loading the standard config file
        try:
            self.load_config( self.parse_config_file(constants.CONFIG) )
        except IOError:
            try:
                # standard config not present? try falling back to presets
                self.load_config( self.parse_config_file(constants.PRESETS) )
            except:
                # hmmmmm, nothing seems to work, so use hardcoded standards
                self.load_config()
        
        # for debugging only
        self.fg.print_config()
        
        # initialize translation
        print('initializing locale...')
        self.init_locale()
        
        # load default text for the textfield from extra file if nothing was saved
        if not self.text:
            try:
                self.get_text_from_file( join(constants.DEFAULT_CONFIG_DIR, self.lang_code) )
            except IOError:
                # file does not exist (happens if no translation is avaible):
                # fall back to english
                self.get_text_from_file( join(constants.DEFAULT_CONFIG_DIR, 'en') )
        
        # initialize data
        print('initializing data...')
        self.init_data(first_run=True)
        
        # and finally raise the GUI
        print('building GUI...')
        self.init_GUI()
        
        print('finished startup')
        self.main_window.mainloop()
        
        
    
    def init_GUI(self):
        self.main_window.title('FGo!')
        # self.main_window.iconbitmap(constants.ICON_PATH)
        
        self.translatedPark = StringVar()
        self.translatedRwy = StringVar()
        self.translatedPark.set(_('None'))
        self.translatedRwy.set(_('Default'))
        
        # Menu ------------------------------------------------------------------
        self.menubar = Menu(self.main_window)

        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label=_('Load'), command=self.load_config)
        self.filemenu.add_command(label=_('Save as...'),
                                  command=self.save_config_file)
        self.filemenu.add_separator()
        self.filemenu.add_command(label=_('Save & Quit'),
                                  command=self.save_and_quit)
        self.filemenu.add_command(label=_('Quit'), command=self.quit)
        self.menubar.add_cascade(label=_('File'), menu=self.filemenu)

        self.settmenu = Menu(self.menubar, tearoff=0)
        self.settmenu.add_checkbutton(label=_('Show installed airports only'),
                                      variable=self.filter_installed_airports,
                                      onvalue=1, offvalue=0,
                                      command=self.init_airport_list)
        self.settmenu.add_separator()
        # self.settmenu.add_command(label=_('Preferences'),
        #                           command=self.showConfigWindow)
        self.menubar.add_cascade(label=_('Settings'), menu=self.settmenu)

        """self.toolsmenu = Menu(self.menubar, tearoff=0)
        self.toolsmenu.add_command(label='METAR',
                                  command=self.showMETARWindow)
        self.menubar.add_cascade(label=_('Tools'), menu=self.toolsmenu)

        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label=_('Help'), command=self.showHelpWindow)
        self.helpmenu.add_separator()
        self.helpmenu.add_command(label=_('About'), command=self.about)
        self.menubar.add_cascade(label=_('Help'), menu=self.helpmenu)
        """
        self.main_window.config(menu=self.menubar)

        self.frame = Frame(self.main_window)
        self.frame.pack(side='top', fill='both', expand=True)

        self.frame0 = Frame(self.frame, borderwidth=4)
        self.frame0.pack(side='top', fill='x')
        
        #------ Aircraft list ---------------------------------------------------------
        self.frame1 = Frame(self.frame0, borderwidth=8)
        self.frame1.pack(side='left', fill='both', expand=True)

        self.frame11 = Frame(self.frame1, borderwidth=1)
        self.frame11.pack(side='top', fill='both', expand=True)
        
        self.aircraft_search_scrollbar = Scrollbar(self.frame11, orient='vertical')
        self.aircraft_listbox = Listbox(self.frame11, bg=constants.TEXT_BG_COL,
                                    exportselection=0,
                                    yscrollcommand=self.aircraft_search_scrollbar.set,
                                    listvariable=self.aircraft_search_list,
                                    height=14)
        self.aircraft_search_scrollbar.config(command=self.aircraft_listbox.yview, takefocus=0)
        self.aircraft_listbox.pack(side='left', fill='both', expand=True)
        self.aircraft_search_scrollbar.pack(side='left', fill='y')
        # self.aircraft_listbox.see(self.getIndex('a'))
        self.aircraft_listbox.bind('<Button-1>', lambda e: self.aircraft_listbox.focus_set())
        self.aircraft_listbox.bind('<<ListboxSelect>>', lambda e: self.update_active_aircraft())
        
        self.frame12 = Frame(self.frame1, borderwidth=1)
        self.frame12.pack(side='top', fill='x')
        
        self.aircraft_search_entry = Entry(self.frame12, bg=constants.TEXT_BG_COL)
        self.aircraft_search_entry.pack(side='left', fill='x', expand=True)
        self.aircraft_search_entry.bind('<Return>', lambda e: self.search_aircrafts())
        self.aircraft_search_entry.bind('<KP_Enter>', lambda e: self.search_aircrafts())

        self.aircraft_search_button = Button(self.frame12, text=_('Search'),
                                      command=self.search_aircrafts)
        self.aircraft_search_button.pack(side='left')
        #------ Middle panel ----------------------------------------------------------
        self.frame2 = Frame(self.frame0, borderwidth=1, relief='sunken')
        self.frame2.pack(side='left', fill='both')
        # Aircraft
        self.frame21 = Frame(self.frame2, borderwidth=4)
        self.frame21.pack(side='top')
        
        self.aircraftLabel = Label(self.frame21,
                                   textvariable=self.active_aircraft_str)
        self.aircraftLabel.pack(side='top')

        self.thumbnail = Label(self.frame21, width=171, height=128)
        self.thumbnail.pack(side='top')
        self.thumbnail.config(image=self.aircraft_image)
        
        # Airport, rwy and parking
        self.frame22 = Frame(self.frame2, borderwidth=4)
        self.frame22.pack(side='top', fill='x')
        
        # First column
        self.frame221 = Frame(self.frame22, borderwidth=4)
        self.frame221.pack(side='left', fill='x')
        
        self.airport_caption = Label(self.frame221, text=_('Airport:'))
        self.airport_caption.pack(side='top')
        
        self.rwy_caption = Label(self.frame221, text=_('start from:'))
        self.rwy_caption.pack(side='top')
        
        self.park_caption = Label(self.frame221, text=_('Position:'))
        self.park_caption.pack(side='top')
        
        # Second column
        self.frame222 = Frame(self.frame22, borderwidth=4)
        self.frame222.pack(side='left', fill='x')
        
        self.airport_label = Label(self.frame222, width=12,
                                  textvariable=self.active_airport_str)
        self.airport_label.pack(side='top')
        # self.airport_label.bind('<Button-1>', self.popupCarrier)
        
        self.starting_pos_type_label = Label(self.frame222, width=12, 
                                             textvariable=self.starting_pos_type, 
                                             relief='groove', borderwidth=2 )
        self.starting_pos_type_label.pack(side='top')
        self.starting_pos_type_label.bind('<Button-1>', 
                                          lambda e: self.create_popup_menu(e.x_root, e.y_root, 'type'))
        
        self.starting_pos_label = Label(self.frame222, width=12,
                              textvariable=self.starting_pos,
                              relief='groove', borderwidth=2)
        self.starting_pos_label.pack(side='top')
        self.starting_pos_label.bind('<Button-1>', 
                               lambda e: self.create_popup_menu(e.x_root, e.y_root, self.starting_pos_type.get()))
        
        # AI Scenarios
        """self.frame23 = Frame(self.frame2)
        self.frame23.pack(side='top')
        
        self.scenarios = Label(self.frame23, text=_('Select Scenario'),
                               relief='groove')
        self.scenarios.pack(side='left')
        self.scenarios.bind('<Button-1>', self.popupScenarios)
        
        self.frame24 = Frame(self.frame2)
        self.frame24.pack(side='top')
        
        self.scenariosLabel = Label(self.frame24,
                                    text=_('Selected Scenarios:'))
        self.scenariosLabel.pack(side='left')

        self.selectedScenarios = IntVar()
        self.countSelectedScenarios()

        self.scenariosCount = Label(self.frame24,
                               textvariable=self.selectedScenarios)
        self.scenariosCount.pack(side='left')"""
        #------ Airport list ----------------------------------------------------------
        self.frame3 = Frame(self.frame0, borderwidth=8)
        self.frame3.pack(side='left', fill='both', expand=True)

        self.frame31 = Frame(self.frame3, borderwidth=1)
        self.frame31.pack(side='top', fill='both', expand=True)

        self.airport_search_scrollbar = Scrollbar(self.frame31, orient='vertical')
        self.airport_listbox = Listbox(self.frame31, bg=constants.TEXT_BG_COL,
                                   exportselection=0,
                                   listvariable=self.airport_search_list,
                                   yscrollcommand=self.airport_search_scrollbar.set,
                                   height=14)
        self.airport_search_scrollbar.config(command=self.airport_listbox.yview, takefocus=0)
        self.airport_listbox.pack(side='left', fill='both', expand=True)
        self.airport_search_scrollbar.pack(side='left', fill='y')
        # self.airport_listbox.see(self.getIndex('p'))
        self.airport_listbox.bind('<Button-1>', lambda e: self.airport_listbox.focus_set())
        self.airport_listbox.bind('<<ListboxSelect>>', lambda e: self.update_active_airport())
        
        self.frame32 = Frame(self.frame3, borderwidth=1)
        self.frame32.pack(side='top', fill='x')
        
        self.airport_search_entry = Entry(self.frame32, bg=constants.TEXT_BG_COL)
        self.airport_search_entry.pack(side='left', fill='x', expand=True)
        self.airport_search_entry.bind('<Return>', lambda e: self.search_airports())
        self.airport_search_entry.bind('<KP_Enter>', lambda e: self.search_airports())
        # self.airport_search_entry.bind('<Any-Key>', self.search_airports)
        
        self.airport_search_button = Button(self.frame32, text=_('Search'),
                                     command=self.search_airports)
        self.airport_search_button.pack(side='left')
        #------ Buttons ---------------------------------------------------------------
        self.frame4 = Frame(self.frame, borderwidth=4)
        self.frame4.pack(side='top', fill='x')
        
        """self.frame41 = Frame(self.frame4, borderwidth=4)
        self.frame41.pack(side='left', fill='x')
        # TerraSync
        self.ts = Checkbutton(self.frame4, text="TerraSync",
                              variable=self.config.TS, command=self.runTS)
        self.ts.pack(side='left')
        """
        self.frame42 = Frame(self.frame4, borderwidth=4)
        self.frame42.pack(side='right')
        # Buttons
        self.sq_button = Button(self.frame42, text=_('Save & Quit'),
                                   command=self.save_and_quit)
        self.sq_button.pack(side='left')
        """
        self.reset_button = Button(self.frame42, text=_('Reset'), width=10,
                                   command=self.reset)
        self.reset_button.pack(side='left')
        """
        self.run_button = Button(self.frame42, text=_('Run FG'), width=10,
                                 command=self.run_flightgear)
        self.run_button.pack(side='left')
        
        #------ Text window -----------------------------------------------------------
        self.frame5 = Frame(self.frame)
        self.frame5.pack(side='top', fill='both', expand=True)
        
        self.frame51 = Frame(self.frame5)
        self.frame51.pack(side='left', fill='both', expand=True)
        
        self.textfield = ScrolledText(self.frame51, bg=constants.TEXT_BG_COL, wrap='none')
        self.textfield.pack(side='left', fill='both', expand=True)
        self.textfield.insert('end', self.text)
        
        #------------------------------------------------------------------------------
        
        #self.scenarioListOpen = False
        #self.currentCarrier = []
        #self.reset(first_run=True)
        #self.runTS()
        
    
    
    def init_locale(self):
        try:
            # initialize provided language...
            self.lang = gettext.translation(constants.MESSAGES, constants.LOCALE_DIR, languages = [self.lang_code])
        except:
            # ... or fallback to system default
            try:
                self.lang = gettext.translation(constants.MESSAGES, constants.LOCALE_DIR)
            except:
                # ... or fallback to english
                self.lang = gettext.translation(constants.MESSAGES, constants.LOCALE_DIR, languages = ['en'])
        
        self.lang.install()
    
    
    def init_data(self, first_run=False):
        self.airport_data  = {}
        self.aircraft_data = {}
        
        # create aircraft database
        print('\tloading aircrafts')
        for aircraft in self.fg.get_aircrafts():
            self.aircraft_data[aircraft.get_name()] = aircraft
        
        # create airport database
        print('\tloading airport database')
        for airport in self.fg.get_airports():
            self.airport_data[airport.get_full_name()] = airport
        
        # initialize the lists for the listboxes
        print('\tgenerating lists')
        self.init_airport_list(first_run=first_run)
        self.init_aircraft_list(first_run=first_run)
        
        print('\tsetting active aircraft/airport')
        # replace the strings in self.active_aircraft/airport with objects
        self.update_active_aircraft(aircraft_str=self.active_aircraft, first_run=first_run, gui=False)
        self.update_active_airport(airport_str=self.active_airport, first_run=first_run)
    
    
    def update_active_aircraft(self, aircraft_str=None, first_run=False, gui=True):
        """update_active_aircraft: updates aircraft name and image
        
        if aircraft_str is not given, 'aircraft_listbox'
        will be used for determining the aircraft
        
        if 'gui' is set, the label containing the aircraft image is updated"""
        
        # if no aircraft name is given, we get it from 'aircraft_listbox'
        if aircraft_str is None and gui:
            aircraft_str = self.aircraft_listbox.get(self.aircraft_listbox.curselection())
        
        # update the aircraft object and its name
        try:
            self.active_aircraft = self.aircraft_data[aircraft_str]
        except KeyError:
            # selected aircraft is not installed
            # this can happen if the default aircraft is not in place
            #
            # in this case we will just choose the first aircraft from the list
            # if any aircrafts are installed at all
            if len(self.aircraft_list) > 0:
                aircraft_str = self.aircraft_list[0]
                self.active_aircraft = self.aircraft_data[aircraft_str]
                
                if gui:
                    self.aircraft_listbox.activate(0)
            else:
                # no aircrafts installed! this is really bad!
                # in this very special case we print an error message into 'active_aircraft_str'
                # and use the fallback image
                self.active_aircraft_str = StringVar(value=_('no aircrafts installed'))
                self.aircraft_image = PhotoImage( file=constants.FALLBACK_AIRCRAFT_IMAGE )
                
                if gui:
                    self.thumbnail.config(image=self.aircraft_image)
                
                return
        
        if first_run:
            self.active_aircraft_str = StringVar(value=aircraft_str)
        else:
            self.active_aircraft_str.set(aircraft_str)
        
        # now lets update the image!
        image_path = self.active_aircraft.get_image_path()
        
        # if no aircraft image is present, use fallback image
        if image_path is None:
            image_path = constants.FALLBACK_AIRCRAFT_IMAGE
        
        self.aircraft_image = PhotoImage(file=image_path)
        
        # at last update the thumbnail-label if GUI is present
        if gui:
            self.thumbnail.config(image=self.aircraft_image)
    
    
    def update_active_airport(self, airport_str=None, first_run=False):
        # if no airport name is given, we get it from the airport_listbox listbox
        if airport_str is None:
            # check if exactly one element is selected
            if len(self.airport_listbox.curselection()) != 1:
                return
            
            airport_str = self.airport_listbox.get(self.airport_listbox.curselection())
        
        # update the airport object and its name
        try:
            self.active_airport = self.airport_data[airport_str]
        except KeyError:
            # the default airport does not seem to be present!
            # normally happens, when 'apt_dat_file' is set incorrectly
            
            # check if any airports are present
            if self.airport_data:
                # if 'airport_data' is not empty, we will activate the first airport
                print('default airport not found!')
                self.active_airport = self.airport_data[self.airport_data.keys()[0]]
            else:
                # no airports present at all!
                print('Warning: no airports found!\nPlease check your scenery path')
                self.active_airport = None
                
        if first_run:
            self.active_airport_str = StringVar(value= (self.active_airport.get_ICAO() if self.active_airport else 'no airports avaible') )
            self.starting_pos_type  = StringVar(value=_('runway'))
            self.starting_pos       = StringVar(value=_('Default'))
        else:
            self.active_airport_str.set(self.active_airport.get_ICAO() if self.active_airport else 'no airports avaible')
            self.starting_pos_type.set(_('runway'))
            self.starting_pos.set(_('Default'))
    
    
    def init_airport_list(self, first_run=False):
        """build a list of airport names from 'self.airport_data'
        and filter the installed airports if neccessary"""
        
        # do we need to show only installed airports?
        if self.filter_installed_airports.get():
            
            # installed_scenery: a list, containing all installed scenery tiles,
            installed_scenery = self.fg.get_installed_scenery_tiles()
            
            # now we check for each airport, whether its scenery tile is contained in 'installed_scenery'
            # if so, it is installed
            filtered_airports = filter(lambda aip: self.airport_data[aip].is_installed(installed_scenery),
                                        self.airport_data.keys())
            self.airport_list = tuple(sorted(filtered_airports))
        else:
            self.airport_list = tuple(sorted(self.airport_data.keys()))
        
        if first_run:
            self.airport_search_list = StringVar(value=self.airport_list)
        else:
            self.airport_search_list.set(value=self.airport_list)
        
        self.last_airport_search = ''
        
        
    
    def init_aircraft_list(self, first_run=False):
        """build a list of aircraft names from 'self.aircraft_data'"""
        
        # create 'self.aircraft_list' as a sorted list of aircraft names
        # key=str.lower makes the program compare the names case unsensitive
        self.aircraft_list = tuple(sorted(self.aircraft_data.keys(), key=str.lower))
        
        if first_run:
            self.aircraft_search_list = StringVar(value=self.aircraft_list)
        else:
            self.aircraft_search_list.set(self.aircraft_list)
    
    
    def search_airports(self, search_text=None):
        """filter all airports from the 'airport_list'
        that contain the 'search_text'
        
        if search_text is not given, it is fetched from the 'airport_search_entry' entry"""
        
        # no 'search_text'? lets get it from 'airport_search_entry'
        if not search_text:
            search_text=self.airport_search_entry.get()
        
        search_text = str(filter(lambda c: c != '\n', search_text))
        # we do not want to search case sensitive, so we change 'search_text' to lowercase
        search_text = search_text.lower()
        
        # 'search_text' has not been changed? then why are we calling this function?
        if search_text == self.last_airport_search:
            return
        
        # generate the new list
        self.airport_search_list.set(value=tuple(filter(lambda s: s.lower().find(search_text) != -1, self.airport_list)))
        # and save the last search text
        self.last_airport_search = search_text
        
    
    def search_aircrafts(self, search_text=None):
        """filter all aircrafts from the 'aircraft_list'
        that contain the 'search_text'"""
        
        if not search_text: search_text=self.aircraft_search_entry.get()
        
        search_text = search_text.lower()
        
        temp_list = tuple(filter(lambda s: s.lower().find(search_text) != -1, self.aircraft_list))
        
        self.aircraft_search_list.set(temp_list)
        
    
    def load_config(self, config={}):
        """ load_config: initialize the program with the options in the dictionary 'config'
        use 'parse_config_file' to create this dictionary from a file """
        
        self.lang_code = config.get('LANG', 'en')
        
        self.fg = FlightGear(root_dir=config.get('--fg-root'),       binary=config.get('FG_BIN'),
                             scenery_dir=config.get('--fg-scenery'), ai_dir=constants.AI_DIR,
                             aircraft_dir=constants.AIRCRAFT_DIR,    airport_dir=constants.AIRPORTS_DIR,
                             apt_dat_file=constants.APT_DAT,         metar_dat_file=constants.METAR_DAT,
                             apt_data_source=int(config.get('APT_DATA_SOURCE', AptDataSource.SCENERY)) )
        
        self.ts = Terrasync(config.get('TERRASYNC_BIN'), port       =config.get('TERRASYNC_PORT'),
                                                         scenery_dir=config.get('TERRASYNC_SCENERY'))
        
        self.active_airport  = config.get('AIRPORT',  constants.DEFAULT_AIRPORT)
        self.active_aircraft = config.get('AIRCRAFT', constants.DEFAULT_AIRCRAFT)
        
        self.filter_installed_airports = BooleanVar(value=bool(int(config.get('FILTER_APT_LIST', 1))))
        
        self.text = config.get('text')
        
        # !!! after this function, 'init_data' has to be called to replace self.active_airport and
        # !!! self.active_aircraft with the corresponding objects from 'airport_data' or 'aircraft_data',
        # !!! which are not in place yet
        
    
    
    def get_config(self, gui=True):
        """returns a dictionary, containing all settings from a running instance of FGo!"""
        config = {}
        
        config['LANG'] = self.lang_code
        
        config['--fg-root']    = self.fg.get_root_dir()
        config['FG_BIN']       = self.fg.get_binary()
        config['--fg-scenery'] = self.fg.get_scenery_dir()
        
        config['APT_DATA_SOURCE'] = self.fg.get_apt_data_source()
        
        config['TERRASYNC_BIN']     = self.ts.get_binary()
        config['TERRASYNC_PORT']    = self.ts.get_port()
        config['TERRASYNC_SCENERY'] = self.ts.get_scenery_dir()
        
        config['AIRCRAFT'] = self.active_aircraft
        config['AIRPORT']  = self.active_airport
        
        config['FILTER_APT_LIST'] = self.filter_installed_airports.get()
        
        if gui:
            config['text'] = self.textfield.get('1.0', END)
        else:
            config['text'] = self.text
        
        return config
        
    
    def parse_config_file(self, config_file):
        # returns a dictionary, containing all options from 'config_file'
        config = open(config_file)
        opts = {}
        
        text = False
        
        for line in config:
            if text:
                opts['text'] += line
                continue
            
            if constants.CUT_LINE in line:
                text = True
                opts['text'] = ''
                continue
                
            if line.startswith('#'):
                continue
            
            i = line.find('=')
            
            if i == -1:
                continue
            
            attribute = line[:i]
            value = line[(i+1):].strip()
            
            if value:
                opts[attribute] = value
            
        return opts
    
    
    def get_text_from_file(self, path):
        # write all the content of the file ('path') in self.text
        
        self.text = ''
        
        file_content = open(path)
        
        if not file_content:
            return
        
        for line in file_content:
            self.text += line
        
        
    
    def parse_textfield(self):
        opts = {}
        
        for line in self.textfield.get('1.0', END).split('\n'):
            line = line.strip()
            
            if not line or line.startswith('#'):
                continue
            
            if line.startswith('--'):
                line = line[2:]
            
            splitted = line.split('=')
            
            if len(splitted) == 1:
                if line.startswith('enable-'):
                    opts[line[7:]] = 1
                elif splitted.startswith('disable-'):
                    opts[line[8:]] = 0
                else:
                    opts[line] = None
            elif len(splitted) == 2:
                opts[splitted[0].strip()] = splitted[1].strip()
        
        return opts
        
    
    def create_popup_menu(self, x_pos, y_pos, type):
        """
        type: 'type' -> select type: runway/parking
              others -> category in starting_positions dictionary
        """
        
        # active airport not set? sorry, but we cannot do anything!
        if not self.active_airport: return
        
        # get a dictionary, containing the runways and parking positions in a sorted form
        starting_positions = self.active_airport.get_starting_positions()
        
        popup = Menu(tearoff=0)
        
        if type == 'type':
            for t in starting_positions.keys():
                if t == 'runway':
                    popup.add_command(label=_('runway'), command=lambda tp=t: self.starting_pos_type.set(tp))
                elif t == 'parking':
                    popup.add_command(label=_('parking'), command=lambda tp=t: self.starting_pos_type.set(tp))
            
            self.starting_pos.set(_('Default'))
            
        else:
            popup.add_command(label=_('Default'), command=lambda: self.starting_pos.set(_('Default')) )
            
            # we need a counter, to start a new column after 20 entries
            i = 1
            
            popup_list = set( self.active_airport.get_starting_positions()[type] )
            
            for rw in sorted(popup_list):
                if i % 20:
                    popup.add_command(label=rw, command=lambda r=rw: self.starting_pos.set(r))
                else:
                    # new column needed
                    popup.add_command(label=rw, command=lambda r=rw: self.starting_pos.set(r), 
                                      columnbreak=1)
                i += 1
        
        
        popup.tk_popup(x_pos, y_pos, 0)
            
    
    def run_flightgear(self):
        opts = self.parse_textfield()
        
        if self.starting_pos_type.get() == 'runway':
            opts['runway']  = self.starting_pos.get()
        elif self.starting_pos_type.get() == 'parking':
            opts['parkpos'] = self.starting_pos.get()
        
        self.fg.start(aircraft=self.active_aircraft,  airport=self.active_airport, opts=opts)
        
    
    def save_config_file(self, config_file=constants.CONFIG):
        config = self.get_config()
        
        output = open(config_file, 'w')
        output.write(constants.CONFIG_HEADER + '\n')
        
        for option in sorted(config.keys()):
            if option != 'text':
                output.write(option + '=' + str(config[option]) + '\n')
        
        output.write(constants.CUT_LINE + '\n')
        
        output.write(config['text'])
        
        output.close()
        
    def quit(self):
        self.main_window.quit()
    
    def save_and_quit(self):
        self.save_config_file()
        self.quit()


class SettingsDialog:
    dialog_captions = {
        '--fg-root'         : _('FlightGear root directory (FG_ROOT)'),
        '--fg-scenery'      : _('FlightGear scenery directory (FG_SCENERY)'),
        'FG_BIN'            : _('FlightGear executable file'),
        'TERRASYNC_BIN'     : _('TerraSync executable file'),
        'TERRASYNC_SCENERY' : _('TerraSync scenery directory'),
        'working_dir'       : _('working directory')
    }
    
    def __init__(self, tk_master, config):
        self.config = config
        
        self.init_GUI(tk_master)
        
    
    def init_GUI(self, tk_master):
        self.top = Toplevel(tk_master)
        
        # settings of the dialogs main window
        self.top.grab_set()  # Focus input on that window.
        self.top.title(_('Preferences'))
        self.top.resizable(width=False, height=False)
        self.top.transient(self.master)
        
        self.main = Frame(self.top, borderwidth=0)
        self.main.pack(side='top', padx=12)
        
        self.frame = Frame(self.main, borderwidth=1, relief='sunken')
        self.frame.pack(side='top')
        
        # ----- Tabs ------------------------------------------------------------------
        self.tabs = Frame(self.frame)
        self.tabs.pack(side='top', fill='x')
        
        self.tabFG = Button(self.tabs, text=_('FlightGear settings'),
                            borderwidth=1, relief='ridge',
                            command=lambda: self.show_tab(self.tabFG))
        self.tabFG.pack(side='left')
        
        self.tabTS = Button(self.tabs, text=_('TerraSync settings'),
                            borderwidth=1, relief='ridge',
                            command=lambda: self.show_tab(self.tabTS))
        self.tabTS.pack(side='left')
        
        self.tabMisc = Button(self.tabs, text=_('Miscellaneous'),
                            borderwidth=1, relief='ridge',
                            command=lambda: self.show_tab(self.tabMisc))
        self.tabMisc.pack(side='left')
        
        # ----- Main content ----------------------------------------------------------
        # Here is placed content from: widgetFG, widgetTS, and widgetMics.
        self.frame = Frame(self.frame, borderwidth=1, relief='raised')
        self.frame.pack(side='top', fill='x')
        
        # ----- Buttons ---------------------------------------------------------------
        self.frame_Buttons = Frame(self.main, borderwidth=12)
        self.frame_Buttons.pack(side='bottom')
        
        self.frame_save_button = Frame(self.frame_Buttons, borderwidth=4)
        self.frame_save_button.pack(side='left')
        
        self.save = Button(self.frame_save_button, text=_('Save settings'),
                           command=self.saveAndQuit)
        self.save.pack(side='left')
        
        self.frame_close_button = Frame(self.frame_Buttons, borderwidth=4)
        self.frame_close_button.pack(side='right')
        
        self.close = Button(self.frame_close_button, text=_('Cancel'),
                            command=self.quit)
        self.close.pack(side='left')
        
        # -----------------------------------------------------------------------------
        # Show FG settings tab by default.
        self.active_tab = None
        
        self.showFGSettings()
    
    
    def search_dialog(self, element):
        try:
            path = fd.askopenfilename(parent=self.top, initialdir=constants.HOME_DIR,
                                      title=SettingsDialog.dialog_captions[element] )
            if path: self.config[element] = path
        
        # abort when 'element' is not one of the needed properties
        except KeyError:
            raise ValueError('\'' + element + '\' is not a path that can be set using this function')
        except TclError:
            return
    
    
    def get_languages(self):
        # return a list of all avaible languages
        res = filter(lambda path: os.path.isdir(os.path.join(LOCALE_DIR, path)), os.listdir(LOCALE_DIR))
        res.sort()
        res.append('-')
        return res
    
    
    def show_tab(self, tab):
        if self.active_tab == tab: return
        
        self.resetTabs()
        tab.configure(borderwidth=1, relief='raised')
        self.cleanUpWidgets()
        
        if tab == self.tabFG:
            self.widgetFG()
        elif tab == self.tabTS:
            self.widgetTS()
        elif tab == self.tabMisc:
            self.widgetMisc()
        
        self.active_tab = tab
        

    def widgetFG(self):
        """FlightGear settings widget."""
        self.frame_FG = Frame(self.frame, borderwidth=8)
        self.frame_FG.pack(side='top')

        self.FG_label = Label(self.frame_FG, text=_('FlightGear settings'))
        self.FG_label.pack(side='top')

        # FG_BIN
        self.frame_FG_1 = Frame(self.frame_FG, borderwidth=4)
        self.frame_FG_1.pack(side='top')

        self.frame_FG_11 = Frame(self.frame_FG_1)
        self.frame_FG_11.pack(side='top', fill='x')

        self.FG_binLabel = Label(self.frame_FG_11,
                                 text=_('Path to executable file:'))
        self.FG_binLabel.pack(side='left')

        self.frame_FG_12 = Frame(self.frame_FG_1)
        self.frame_FG_12.pack(side='top')

        self.FG_binEntry = Entry(self.frame_FG_12, bg=TEXT_BG_COL,
                                 width=50, textvariable=self.FG_bin)
        self.FG_binEntry.pack(side='left')

        self.FG_binFind = Button(self.frame_FG_12, text=_('Find'),
                                 command=lambda: self.search_dialog('FG_BIN'))
        self.FG_binFind.pack(side='left')
        # FG_ROOT
        self.frame_FG_2 = Frame(self.frame_FG, borderwidth=4)
        self.frame_FG_2.pack(side='top')

        self.frame_FG_21 = Frame(self.frame_FG_2)
        self.frame_FG_21.pack(side='top', fill='x')

        self.FG_rootLabel = Label(self.frame_FG_21, text='FG_ROOT:')
        self.FG_rootLabel.pack(side='left')

        self.frame_FG_22 = Frame(self.frame_FG_2)
        self.frame_FG_22.pack(side='top')

        self.FG_rootEntry = Entry(self.frame_FG_22, bg=TEXT_BG_COL,
                                  width=50, textvariable=self.FG_root)
        self.FG_rootEntry.pack(side='left')

        self.FG_rootFind = Button(self.frame_FG_22, text=_('Find'),
                                  command=lambda: self.search_dialog('--fg-root'))
        self.FG_rootFind.pack(side='left')
        # FG_SCENERY
        self.frame_FG_3 = Frame(self.frame_FG, borderwidth=4)
        self.frame_FG_3.pack(side='top')

        self.frame_FG_31 = Frame(self.frame_FG_3)
        self.frame_FG_31.pack(side='top', fill='x')

        self.FG_sceneryLabel = Label(self.frame_FG_31, text='FG_SCENERY:')
        self.FG_sceneryLabel.pack(side='left')

        self.frame_FG_32 = Frame(self.frame_FG_3)
        self.frame_FG_32.pack(side='top')

        self.FG_sceneryEntry = Entry(self.frame_FG_32, bg=TEXT_BG_COL,
                                  width=50, textvariable=self.FG_scenery)
        self.FG_sceneryEntry.pack(side='left')

        self.FG_sceneryFind = Button(self.frame_FG_32, text=_('Find'),
                                     command=lambda: self.search_dialog('--fg-scenery'))
        self.FG_sceneryFind.pack(side='left')

        # FG working directory
        self.frame_FG_4 = Frame(self.frame_FG, borderwidth=4)
        self.frame_FG_4.pack(side='top')

        self.frame_FG_41 = Frame(self.frame_FG_4)
        self.frame_FG_41.pack(side='top', fill='x')

        self.FG_working_dirLabel = Label(self.frame_FG_41,
                                       text=_('Working directory (optional):'))
        self.FG_working_dirLabel.pack(side='left')

        self.frame_FG_42 = Frame(self.frame_FG_4)
        self.frame_FG_42.pack(side='top')

        self.FG_working_dirEntry = Entry(self.frame_FG_42, bg=TEXT_BG_COL,
                               width=50, textvariable=self.FG_working_dir)
        self.FG_working_dirEntry.pack(side='left')

        self.FG_working_dirFind = Button(self.frame_FG_42, text=_('Find'),
                               command=lambda: self.search_dialog('working_dir'))
        self.FG_working_dirFind.pack(side='left')

    def widgetTS(self):
        """TerraSync settings widget."""
        self.frame_TS = Frame(self.frame, borderwidth=8)
        self.frame_TS.pack(side='top')

        self.TS_label = Label(self.frame_TS, text=_('TerraSync settings'))
        self.TS_label.pack(side='top')

        # TS_BIN
        self.frame_TS_1 = Frame(self.frame_TS, borderwidth=4)
        self.frame_TS_1.pack(side='top')

        self.frame_TS_11 = Frame(self.frame_TS_1)
        self.frame_TS_11.pack(side='top', fill='x')

        self.TS_binLabel = Label(self.frame_TS_11,
                                 text=_('Path to executable file:'))
        self.TS_binLabel.pack(side='left')

        self.frame_TS_12 = Frame(self.frame_TS_1)
        self.frame_TS_12.pack(side='top')

        self.TS_binEntry = Entry(self.frame_TS_12, bg=TEXT_BG_COL,
                                 width=50, textvariable=self.TS_bin)
        self.TS_binEntry.pack(side='left')

        self.TS_binFind = Button(self.frame_TS_12, text=_('Find'),
                                 command=lambda: self.search_dialog('TERRASYNC_BIN'))
        self.TS_binFind.pack(side='left')
        # TS scenery
        self.frame_TS_2 = Frame(self.frame_TS, borderwidth=4)
        self.frame_TS_2.pack(side='top')

        self.frame_TS_21 = Frame(self.frame_TS_2)
        self.frame_TS_21.pack(side='top', fill='x')

        self.TS_sceneryLabel = Label(self.frame_TS_21, text=_('Scenery path:'))
        self.TS_sceneryLabel.pack(side='left')

        self.frame_TS_22 = Frame(self.frame_TS_2)
        self.frame_TS_22.pack(side='top')

        self.TS_sceneryEntry = Entry(self.frame_TS_22, bg=TEXT_BG_COL,
                                     width=50, textvariable=self.TS_scenery)
        self.TS_sceneryEntry.pack(side='left')

        self.TS_sceneryFind = Button(self.frame_TS_22, text=_('Find'),
                                     command=lambda: self.search_dialog('TERRASYNC_SCENERY'))
        self.TS_sceneryFind.pack(side='left')
        # TS port
        self.frame_TS_3 = Frame(self.frame_TS, borderwidth=4)
        self.frame_TS_3.pack(side='top', fill='x')

        self.frame_TS_31 = Frame(self.frame_TS_3)
        self.frame_TS_31.pack(side='top', fill='x')

        self.TS_portLabel = Label(self.frame_TS_31, text=_('Port:'))
        self.TS_portLabel.pack(side='left')

        self.frame_TS_32 = Frame(self.frame_TS_3)
        self.frame_TS_32.pack(side='top', fill='x')

        self.TS_portEntry = Entry(self.frame_TS_32, bg=TEXT_BG_COL,
                                  width=6, textvariable=self.TS_port)
        self.TS_portEntry.pack(side='left')

        self.TS_portDefault = Button(self.frame_TS_32, text=_('Default'),
                                     command=self.defaultPort)
        self.TS_portDefault.pack(side='left')

    def widgetMisc(self):
        """Miscellaneous settings widget."""
        self.frame_misc = Frame(self.frame, borderwidth=8)
        self.frame_misc.pack(side='top', fill='x')

        self.misc_label = Label(self.frame_misc, text=_('Miscellaneous'))
        self.misc_label.pack(side='top', fill='x')

        # Language menu
        self.frame_misc_1 = Frame(self.frame_misc, borderwidth=4)
        self.frame_misc_1.pack(side='top', fill='x')

        self.frame_misc_11 = Frame(self.frame_misc_1)
        self.frame_misc_11.pack(side='left', fill='x')

        self.lang_label = Label(self.frame_misc_11, text=_('Change language:'))
        self.lang_label.pack(side='left')

        self.langMenu = OptionMenu(self.frame_misc_11, self.language,
                                   *self.get_languages())
        self.langMenu.pack(side='left')
        # Apt source menu
        self.frame_misc_12 = Frame(self.frame_misc_1)
        self.frame_misc_12.pack(side='right', fill='x')

        self.apt_label = Label(self.frame_misc_12,
                               text=_('Airport data source:'))
        self.apt_label.pack(side='left')

        self.aptMenu = OptionMenu(self.frame_misc_12, self.apt_data_source,
                                  *(_('Default'), _('Scenery')))
        self.aptMenu.pack(side='left')
        # Rebuild apt button
        self.frame_misc_2 = Frame(self.frame_misc, borderwidth=4)
        self.frame_misc_2.pack(side='top', fill='x')

        self.frame_misc_21 = Frame(self.frame_misc_2)
        self.frame_misc_21.pack(side='top', fill='x')

        self.rebuildApt = Button(self.frame_misc_21,
                                 text=_('Rebuild Airport Database'),
                                 command=self.config.rebuildApt)
        self.rebuildApt.pack(side='top', fill='x')

if __name__ == "__main__":
    test = FGo()
    
