# -*- coding: utf-8 -*-
"""global constants for use with FGo!"""


from os.path import expanduser, join


# FGo! related constants.
NAME = 'FGo! 1.3.9'
COPYRIGHT = "Copyright 2009-2011 by\nRobert 'erobo' Leda  <erobo@wp.pl> and Raphael Duemig"
LICENSE = \
"""This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.
"""
HOME_DIR = expanduser('~')
DATA_DIR = join(HOME_DIR, '.fgo')  # Default directory for all configuration files.
APT = join(DATA_DIR, 'apt')  # Path to airport data file.
INSTALLED_APT = join(DATA_DIR, 'apt_installed')  # Path to locally installed airport list.
CONFIG = join(DATA_DIR, 'config')  # Path to config file.
DEFAULT_CONFIG_DIR = '../config'  # Name of default config directory.
PRESETS = join(DEFAULT_CONFIG_DIR, 'presets')  # Path to config file with predefined settings.
HELP_DIR = 'help'  # Name of help directory.
LOCALE_DIR = '../locale'  # Name of directory where localization files are stored.
MESSAGES = 'messages'  # Name of localization file.
IMAGES_DIR = '../pics'
ICON_PATH = join(IMAGES_DIR, 'fgo.ico')
FALLBACK_AIRCRAFT_IMAGE = join(IMAGES_DIR, 'thumbnail.jpg')  # Path to substitutionary thumbnail.
CONFIG_HEADER = '# created by ' + NAME
CUT_LINE = ' INTERNAL OPTIONS ABOVE. EDIT CAREFULLY! '.center(80, 'x')
DEFAULT_AIRCRAFT = 'c172p'
DEFAULT_AIRPORT = 'KSFO San Francisco Intl'
# Custom colors.
CARRIER_COL = '#98afd9'  # Color to highlight background for carrier name in the main window.
COMMENT_COL = '#0014a7'  # Color to highlight comments in text window.
GRAYED_OUT_COL = '#b2b2b2'  # Color to apply to rwy button when inactive.
MESSAGE_BG_COL = '#fffe98'  # Background color for various messages.
TEXT_BG_COL = '#ffffff'  # Background color for various text windows.

# FG related constants.
AI_DIR = 'AI'  # FG_DATA/AI directory name.
AIRCRAFT_DIR = 'Aircraft'  # FG_DATA/Aircraft directory name.
AIRPORTS_DIR = 'Airports'  # FG_DATA(or FG_SCENERY)/Airports directory name.
APT_DAT = join('Airports', 'apt.dat.gz')  # FG_DATA/Airports/apt.dat.gz file path.
METAR_DAT = join('Airports', 'metar.dat.gz')  # FG_DATA/Airports/apt.dat.gz file path.
