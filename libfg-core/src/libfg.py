#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os.path import isdir, isfile, join
import subprocess

import math

# data structures and algorithms for all the x-plane data files (apt_dat, nav_dat, ...)
from   xplane_dat.data import *
import xplane_dat.file_parsing

from fg_data.data import *
import fg_data.file_parsing

import libfg_defaults


def get_scenery_tile(pos):
    # get the name of the scenery tile, which contains this position
    
    lat_str = ('n' if pos.latitude  > 0 else 's') + '%02d' % math.fabs(math.floor(pos.latitude ))
    lon_str = ('e' if pos.longitude > 0 else 'w') + '%03d' % math.fabs(math.floor(pos.longitude))
    
    return lon_str + lat_str



class FlightGear:
    """structure to collect all paths that are needed, when working with
    FlightGear installations
    
    root_dir:       FG_ROOT
    binary:         flightgear executable
    scenery_dir:    FG_SCENERY
    ai_dir:         usually FG_ROOT/AI
    aircraft_dir:   usually FG_ROOT/Aircraft
    aiport_dir:     airport directory inside 'ai_dir'
    apt_dat_file:   apt.dat.gz file
    fix_dat_file:   fix.dat.gz file
    metar_dat_file: metar.dat.gz file
    apt_data_source:    read airport data from 'scenery_dir'(default)
                        or from 'airport_dir'
    """
    
    class AptDataSource:
        SCENERY = 1
        AI_DATA = 0
    
    
    def __init__(self, root_dir=None, binary=None, scenery_dir=None, ai_dir=None, aircraft_dir=None,
                 airport_dir=None, apt_dat_file=None, fix_dat_file=None, metar_dat_file=None,
                 apt_data_source=None):
        
        # if 'root_dir' and 'binary' are not given, we try to get them by testing popular paths
        self.root_dir = root_dir if root_dir else guess_path(libfg_defaults.root_dir_guess_list) 
        self.binary   = binary   if binary   else guess_path(libfg_defaults.binary_guess_list) 
        
        if self.root_dir:
            # apply defaults if nothing worthy is given
            if not scenery_dir:     scenery_dir     = libfg_defaults.scenery_dir
            if not ai_dir:          ai_dir          = libfg_defaults.ai_dir
            if not aircraft_dir:    aircraft_dir    = libfg_defaults.aircraft_dir
            if not airport_dir:     airport_dir     = libfg_defaults.airport_dir
            if not apt_dat_file:    apt_dat_file    = libfg_defaults.apt_dat_file
            if not fix_dat_file:    fix_dat_file    = libfg_defaults.fix_dat_file
            if not metar_dat_file:  metar_dat_file  = libfg_defaults.metar_dat_file
            
            # if given pats (except binary) are relative, we will append them to root_dir
            self.scenery_dir  = join(self.root_dir, scenery_dir)
            self.ai_dir       = join(self.root_dir, ai_dir)
            self.aircraft_dir = join(self.root_dir, aircraft_dir)
            self.airport_dir  = join(self.root_dir, airport_dir)
            
            self.apt_dat_file   = join(self.root_dir, apt_dat_file)
            self.fix_dat_file   = join(self.root_dir, fix_dat_file)
            self.metar_dat_file = join(self.root_dir, metar_dat_file)
        else:
            print('libfg: WARNING: no valid FG root directory found')
            
            # in case we do not know FG_ROOT, we are only able to deal with the absolute paths
            self.scenery_dir    = scenery_dir    if scenery_dir    and    scenery_dir.startswith('/') else ''
            self.ai_dir         = ai_dir         if ai_dir         and         ai_dir.startswith('/') else ''
            self.aircraft_dir   = aircraft_dir   if aircraft_dir   and   aircraft_dir.startswith('/') else ''
            self.airport_dir    = airport_dir    if airport_dir    and    airport_dir.startswith('/') else ''
            self.apt_dat_file   = apt_dat_file   if apt_dat_file   and   apt_dat_file.startswith('/') else ''
            self.fix_dat_file   = fix_dat_file   if fix_dat_file   and   fix_dat_file.startswith('/') else ''
            self.metar_dat_file = metar_dat_file if metar_dat_file and metar_dat_file.startswith('/') else ''
        
        self.apt_data_source = apt_data_source if apt_data_source else self.AptDataSource.SCENERY
        
    
    # getter functions
    def get_root_dir(self):
        return self.root_dir
    
    def get_binary(self):
        return self.binary
    
    def get_scenery_dir(self):
        return self.scenery_dir
    
    def get_ai_dir(self):
        return self.ai_dir
    
    def get_aircraft_dir(self):
        return self.aircraft_dir
    
    def get_apt_dat_file(self):
        return self.apt_dat_file
    
    def get_metar_dat_file(self):
        return self.metar_dat_file
    
    def get_apt_data_source(self):
        return self.apt_data_source
    
    
    #
    # functions dealing with aircrafts
    #
    def get_aircrafts(self):
        """search for *-set.xml files in 'aircraft_dir' and return an
        iterator on the aircaft objects"""
        
        try:
            for aircraft_group in os.listdir(self.aircraft_dir):
                path = join(self.aircraft_dir, aircraft_group)
                if not os.path.isdir(path): continue
                
                for aircraft_file in filter(lambda f: f[-8:]=='-set.xml', os.listdir(path)):
                    aircraft_name = aircraft_file[:-8]
                    
                    if aircraft_group + '/' + aircraft_name not in libfg_defaults.ignored_aircrafts:
                        yield Aircraft(aircraft_name, aircraft_group)
            
        except OSError:
            return
        
    
    def get_aircraft_image_path(self, aircraft):
        return join(self.aircraft_dir, aircraft.get_group(), libfg_defaults.aircraft_image_file)
    
    
    #
    # functions dealing with airports
    #
    def get_airports(self):
        return xplane_dat.file_parsing.get_airports(self.apt_dat_file)
    
    
    def get_airport_dir(self, airport):
        if airport.__class__ == Airport:
            airport = airport.get_ICAO()
        
        if self.apt_data_source == self.AptDataSource.SCENERY:
            path = join(self.scenery_dir, "Airports", airport[0], airport[1], airport[2])
        else:
            path = join(self.airport_dir, airport)
        
        # heliports do neither have a threshold nor a parking position, so it can happen,
        # that the directory does not exist
        return path if isdir(path) else None
    
    
    def get_groundnet_file(self, airport):
        """get the groundnet.xml or parking.xml file, which belongs to
        'airport'
        
        airport: can be either an Airport object, or a string, consisting
        of an ICAO code
        """
        
        icao = airport.get_ICAO() if airport.__class__ == Airport else airport
        path = self.get_airport_dir(icao)
        
        if path is None: return None
        
        if os.path.isfile( join(path, airport + '.' + libfg_defaults.airport_groundnet_file) ):
            return join(path, airport + '.' + libfg_defaults.airport_groundnet_file)
        elif os.path.isfile( join(path, airport + '.' + libfg_defaults.airport_oldstyle_groundnet_file) ):
            return join(path, airport + '.' + libfg_defaults.airport_oldstyle_groundnet_file)
        else:
            return None
    
    
    def airport_has_metar_info(self, airport):
        icao = airport.get_ICAO() if airport.__class__ == Airport else airport
        
        return icao in self.get_metar_airports()
    
    
    def get_parking_positions(self, airport):
        """parse the groundnet file returned by
        'self.getGroundnetFile(airport)' and extract name + number of
        each parking position"""
        
        icao = airport.get_ICAO() if airport.__class__ == Airport else airport
        
        f = self.get_groundnet_file(icao)
        
        return fg_data.file_parsing.get_parking_positions(f)
    
    
    def get_threshold_file(self, airport):
        if airport.__class__ == Airport:
            airport = airport.get_ICAO()
        
        airport_dir = self.get_airport_dir(airport)
        
        if airport_dir is None: return None
        
        path = join(self.get_airport_dir(airport), libfg_defaults.airport_threshold_file)
        
        # heliports do not have a threshold file, mind this!
        return path if isfile(path) else None
        
    
    def get_thresholds(self, airport):
        
        # if the type of airport is the airport class, convert it to its ICAO-code
        icao = airport.get_ICAO() if airport.__class__ == Airport else airport
        
        path = self.get_threshold_file(icao)
        
        if path is None:
            return
        else:
            return fg_data.file_parsing.get_thresholds(path)
    
    
    def get_installed_scenery_tiles(self):
        """returns a list with all directories found in
        'scenery_dir'/Terrain/*/"""
        
        terrain_dir = self.scenery_dir + "/Terrain"
        
        if not isdir(terrain_dir):
            return []
        
        scenery_tiles = []
        
        for directory in os.listdir(terrain_dir):
            scenery_tiles.extend(os.listdir(terrain_dir + "/" + directory))
        
        return scenery_tiles
    
    
    def get_fixes(self):
        return xplane_dat.file_parsing.get_fixes(self.fix_dat_file)
    
    
    def get_metar_airports(self):
        return fg_data.file_parsing.get_metar_airports(self.metar_dat_file)
    
    
    def get_scenario_list(self):
        """Walk through AI scenarios and read their data.
        returns list of all scenarios"""
        scenarios = []
        
        try:
            for f in filter(lambda f: f.endswith('.xml'), os.listdir(self.ai_dir)):
                scenarios.append( fg_data.file_parsing.get_scenario(os.path.join(self.ai_dir, f)) )
            
        except OSError:
            pass
        
        return scenarios
    
    
    def print_config(self):
        # prints all paths to stdout
        # 
        # used for debugging
        
        print('root_dir: ' + self.root_dir)
        print('scenery_dir: ' + self.scenery_dir)
        print('ai_dir: ' + self.ai_dir)
        print('aircraft_dir: ' + self.aircraft_dir)
        print('airport_dir: ' + self.airport_dir)
        print('apt_dat_file: ' + self.apt_dat_file)
        print('metar_dat_file: ' + self.metar_dat_file)
        
    
    def start(self, aircraft  = None, airport = None, carrier = None, scenarios = [],
                    terrasync = None, opts    = {},   opt_str = ''):
        """launch fgfs
        
        opts: options can use dictionary-form: 
        'timeofday'  :'noon' ==> '--timeofday=noon'
        'fullscreen' :1      ==> '--enable-fullscreen'
        'clouds'     :0      ==> '--disable-clouds'
        'fog-fastest':''     ==> '--fog-fastest'
        
        alternative:
        opt_str: '--timeofday=noon --enable-fullscreen --fog-fastest'
        
        special options aircraft, airport, scenario accept the
        corresponding objects (Aircraft, Airport, etc.)"""
        
        command = [ self.binary, "--fg-root=%s" % self.root_dir ]
        
        if not terrasync:
            command.append('--fg-scenery=%s' % self.scenery_dir)
        else:
            command.append('--fg-scenery=%s' % terrasync.getSceneryDir())
            command.append('--atlas=socket,out,1,localhost,%d,udp' % terrasync.getPort())
        
        if aircraft:
            opts['aircraft'] = aircraft.get_name()
        if airport:
            print(type(airport.get_ICAO()))
            opts['airport']  = airport.get_ICAO()
        if carrier:
            opts['carrier']  = carrier.get_name()
        
        scenarios = set(scenarios) # remove doubles
        
        for scenario in scenarios:
            command.append('--ai-scenario=%s' % scenario.get_name())
        
        for attribute in sorted(opts.keys()):
            arg = opts[attribute]
            
            if arg.__class__ == int:
                if arg:
                    command.append('--enable-' + attribute)
                else:
                    command.append('--disable-' + attribute)
            elif arg:
                command.append('--' + attribute + '=' + arg)
            else:
                command.append('--' + attribute)
        
        if opt_str:
            command.append(opt_str)
        
        print(command)
        return subprocess.Popen(command)
    

def guess_path(guess_list):
    for path in guess_list:
        if isdir(path) or isfile(path):
            return path
    
    return ''



#if __name__ == "__main__":
#    import sys
#    fg = FlightGear("/usr/share/FlightGear/data", binary="/usr/bin/fgfs")
#    airport = None
#    for aip in fg.get_airports():
#        if aip.get_ICAO() == sys.argv[1]:
#            airport = aip
#            break
#    print('next metar station to ' + sys.argv[1] + ': ' + airport.getClosestMetarStation(fg.getAirports()).getICAO())
#    
#    print("\ncarriers:")
#    for carrier in carriers:
#        print(carrier.info())

    # aircrafts = fg.getAircraftList()
    
    #ts = libts.Terrasync('/usr/bin/terrasync', ts_scenery='/home/raphael/.fgfs/terrasync', port=5501)
    #ts.start()
    #
    #command = fg.start(aircraft=aircrafts[8], airport=airports[15], terrasync=ts, opts={'fullscreen':0, 'timeofday':'dawn'})
    #print('starting FlightGear with the following command:')
    #print(command)
    #fg.wait()
    #ts.stop()
    
