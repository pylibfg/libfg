# -*- coding: utf-8 -*-
# default values for libfg.py, a python library for reading FlightGear data files

root_dir_guess_list = [ "/usr/local/share/FlightGear/data", "/usr/share/FlightGear/data",
                       "/usr/share/games/flightgear", "/usr/share/games/FlightGear" ]
binary_guess_list = [ "/usr/local/bin/fgfs", "/usr/bin/fgfs" ]

# paths can be relative to 'root_dir' (alias FG_ROOT)
ai_dir       = "AI"
aircraft_dir = "Aircraft"
airport_dir  = ai_dir + "/Airports"
scenery_dir  = "Scenery"

apt_dat_file   = "Airports/apt.dat.gz"
metar_dat_file = "Airports/metar.dat.gz"
fix_dat_file   = "Navaids/fix.dat.gz"
nav_dat_file   = "Navaids/nav.dat.gz"

# ------- aircrafts ---------

# relative to directory of the specific aircraft
aircraft_image_file = "thumbnail.jpg"
# add here set files, which do not contain an aircraft (path relative to 'aircraft_dir' but without -set.xml ending)
ignored_aircrafts   = [ "seahawk/carrier" ]

# ------- airports  ---------

# relative to directory of the specific airport
airport_threshold_file          = "threshold.xml"
airport_groundnet_file          = "groundnet.xml"
airport_oldstyle_groundnet_file = "parking.xml"

# ------- Terrasync ---------

ts_port = 5500
