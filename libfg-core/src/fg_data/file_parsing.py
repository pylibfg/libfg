'''
Created on 16.07.2012

@author: raphael
'''

import gzip
import os

from fg_data.data import Carrier, Scenario


def get_parking_positions(groundnet_file):
        """parse the groundnet file returned by and extract name + number of
        each parking position"""
        
        # no groundnet.xml / parking.xml file present?
        if not os.path.isfile(groundnet_file): return
        
        fin = open(groundnet_file)
        
        entry = ""
        name = number = ""
        finished = False
        
        for line in fin:
            if not entry:
                i = line.find('<Parking ')
                
                if i == -1:
                    continue
                
                j = line.find('/>')
                if j == -1 or j < i:
                    entry = line[i+9:]
                else:
                    entry = line[i+9:j]
                    finished = True
            else:
                j = line.find('/>')
                if j == -1:
                    entry += line
                else:
                    entry += line[:j]
                    finished = True
            
            if finished:
                while True:
                    entry = entry.strip()
                    
                    if not entry:
                        break
                    
                    i = 0
                    j = 0
                    while entry[j] != '=': j += 1
                    att = entry[i:j]
                    
                    i = j = j + 2
                    while entry[j] != '"': j += 1
                    arg = entry[i:j]
                    
                    if att == 'name':
                        name = arg
                    elif att == 'number':
                        number = arg
                    
                    entry = entry[j+1:]
                
                if name and number:
                    yield name + number
                
                name = number = entry = ''
                finished = False
        
        fin.close()


def get_scenario(scenario_file):
    scenario_name = os.path.basename(scenario_file)[:-4]
    
    fin = open(scenario_file)
    
    p_carriers = []
    carrier_name = ""
    carrier_parking = []
    is_carrier = False
    scenario_description = ""
    is_description = False
    
    for line in fin:
        line = line.strip()
        
        if is_carrier:
            i = line.find('<name>')
            
            if i != -1:
                j = line.find('</name>')
                
                if not carrier_name:
                    carrier_name = line[i+6:j].strip()
                else:
                    carrier_parking.append(line[i+6:j].strip())
            if '</entry>' in line:
                p_carriers.append( Carrier(name=carrier_name, parking=carrier_parking) )
                carrier_name = ""
                carrier_parking = []
                is_carrier = False
        
        elif is_description:
            j = line.find('</description>')
            if j == -1:
                if not line:
                    scenario_description = ''.join([scenario_description, '\n'])
                else:
                    scenario_description = ' '.join([scenario_description, line])    
            else:
                scenario_description = ' '.join([scenario_description, line[:j].strip()])
                is_description = False
        
        else:
            i = line.find('<type>')
            if i != -1:
                j = line.find('</type>')
                
                if line[i+6:j].strip() == 'carrier':
                    is_carrier = True
            
            i = line.find('<description>')
            if i != -1:
                j = line.find('</description>')
                
                if j == -1:
                    scenario_description = line[i+13:]
                    is_description = True
                else:
                    scenario_description = line[i+13:j]
        
    fin.close()
    
    return Scenario(name=scenario_name, description=scenario_description, carriers=p_carriers)
                    

def get_metar_airports(metar_dat_file):
    """get METAR station list from metar.dat.gz file"""
    
    res = []
    
    try:
        fin = gzip.open(metar_dat_file)
        
        for line in fin:
            line = line.decode('iso-8859-1')
            if line.startswith('#'): continue
            
            line = line.strip()
            res.append(line)
        
        fin.close()
        
    except IOError:
        pass
    
    return res


def get_thresholds(threshold_file):
    f = open(threshold_file)
    
    for line in f:
        i = line.find('<rwy>')
        
        if i == -1: continue
        
        j = line.find('</rwy>')
        
        yield line[i+5:j].strip()
    
    return

