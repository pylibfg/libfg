'''
Created on 16.07.2012

@author: raphael
'''


class Aircraft:
    def __init__(self, name='', group=''):
        self.name  = name
        self.group = group
    
    def get_name(self):
        return self.name
    
    def get_group(self):
        return self.group
    
    def info(self):
        return self.name + ":\t" + self.group


class Scenario:
    def __init__(self, name="", description="", carriers=[]):
        self.name        = name
        self.description = description
        self.carriers    = carriers
    
    def get_description(self):
        return self.description
    
    def get_name(self):
        return self.name
    
    def info(self):
        res = self.name + ':\n'
        
        for car in self.carriers:
            res += car.info() + '\n'
        return res


class Carrier:
    def __init__(self, name="", parking=[]):
        self.name     = name
        self.parking  = parking
    
    def get_name(self):
        return self.name
    
    def info(self):
        res = self.name + ":\t"
        
        for parking_pos in self.parking:
            res += parking_pos + ' '
        return res
    
    def get_starting_positions(self):
        return { 'parking' : self.parking }

