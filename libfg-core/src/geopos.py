'''
Created on 21.11.2012

@author: raphael
'''


# for position calculations:
import math


class Position:
    def __init__(self, latitude, longitude):
        self.latitude  = latitude
        self.longitude = longitude
    
    
    def distance_to(self, other):
        # returns the smallest arc between the two coordinates in radians
        # if you want to get the distance, multiply the result with the radius of the earth in your measurement
        
        pos_self  = ( math.radians( self.latitude), math.radians( self.longitude) )
        pos_other = ( math.radians(other.latitude), math.radians(other.longitude) )
        
        return math.acos( math.sin(pos_self[0]) * math.sin(pos_other[0]) +
                          math.cos(pos_self[0]) * math.cos(pos_other[0]) * math.cos(pos_other[1] - pos_self[1]) )

    def closest(self, others):
        # returns the position from #others, that is closest to this position
        min_dist = math.pi
        result = None

        for other in others:
            dist = self.distance_to(other)
            if dist < min_dist:
                min_dist = dist
                result = other
        return result
    
    def pos_tuple(self):
        return (self.latitude, self.longitude)


