'''
Created on 16.07.2012

@author: raphael
'''

from xplane_dat.data import Airport, Fix

import os
import gzip


def get_airports(apt_dat_file):
    """iterates over all airports from 'apt_dat_file'"""
    
    if not os.path.isfile(apt_dat_file):
        return
    
    fin = gzip.open(apt_dat_file)
    
    found = False
    lat, lon = 0.0, 0.0
    
    for line in fin:
        # apt.dat is apparently using iso-8859-1 coding,
        # so we need to decode it to unicode.
        line = line.decode('iso-8859-1')
        
        ls = line.split()
        try:
            code = int( line.split()[0] )
        except (IndexError, ValueError):
            code = None
        
        # Find ICAO and airport name.
        if code in (1, 16, 17):
            e = line.strip().split()
            
            icao_p = e[4]
            name_p = ' '.join(e[5:])
            rwys_p = []
            found = True
            continue
        
        # Find runways (entries with 100, 101, 102 rowcode)
        elif code in (100, 101, 102):
            if code == 100:
                e = line.strip().split()[8:]
            elif code == 101:
                e = line.strip().split()[3:]
            else:
                e = line.strip().split()[1:]
            
            rwy_id = e[0]
            
            if rwy_id.endswith('x'):
                rwy_id = rwy_id[:-1]
            
            lat = float(e[1])
            lon = float(e[2])
            
            rwys_p.append(rwy_id)
        
        elif code == 101:
            e = line.strip().split()
            
        elif not line.strip() and found:
            coords_p = (lat, lon)
            
            yield Airport(icao=icao_p, name=name_p, runways=rwys_p, coords=coords_p)
    
    
    # finally:
    fin.close()
    
    # apt.dat.gz has no '\n' character at the end of the file, so we will have to execute
    # the last lines again, to save the last airport
    
    if found:
        coords_p = (lat, lon)
        
        yield Airport(icao=icao_p, name=name_p, runways=rwys_p, coords=coords_p)
    

def get_fixes(fix_dat_file):
    """iterates over all fixes from 'fix_dat_file'"""
        
    if not os.path.isfile(fix_dat_file):
        return
    
    fin = gzip.open(fix_dat_file)
    
    for line in fin:
        # fix.dat is apparently using iso-8859-1 coding
        line = line.decode('iso-8859-1')
        
        splitted = line.split()
        
        if len(splitted) != 3: continue
        
        # splitted[0] : latitude
        # splitted[1] : longitude
        # splitted[2] : name
        yield Fix(splitted[2], float(splitted[0]), float(splitted[1]))
    
    fin.close()

