'''
Created on 16.07.2012

@author: raphael
'''

import math

from geopos import Position


class Fix(Position):
    def __init__(self, name, lat, lon):
        Position.__init__(self, lat, lon)
        self.name = name
    
    def get_name(self):
        return self.name


class Navaid(Fix):
    def __init__(self, name, lat, lon, nav_type, freq):
        Fix.__init__(self, name, lat, lon)
        self.type = nav_type
        self.freq = freq
    
    def get_type(self):
        return self.type
    
    def get_frequency(self):
        return self.freq


class Airport(Position):
    """ small helper class to encapsulate airport data """
    
    def __init__(self, flightgear=None, icao="", name="", runways=None, parking_positions=None, coords=(0,0), metar=False):
        # coords: format: (latitude, longitude)
        Position.__init__(self, coords[0], coords[1])
        
        # fg: instance of FlightGear class -- helps us locating and parsing the data files
        self.fg      = flightgear
        
        self.icao    = icao
        self.name    = name
        
        self.runways = runways
        self.parking = parking_positions
        
        self.metar  = metar
        
    
    def get_ICAO(self):
        return self.icao
    

    def get_name(self):
        return self.name
    
    
    def get_full_name(self):
        if len(self.icao) == 3:
            return self.icao + '  ' + self.name
        else:
            return self.icao + ' ' + self.name
    
    
    def get_runway_list(self):
        
        all_runways = []
        
        for rw in self.runways:
            try:
                rw_dir = int(rw[0:2])
            except ValueError:
                # runway does not seem to begin with a number
                # normally happens with heliports
                
                all_runways.append(rw)
                continue
            
            if rw_dir <= 18:
                opp_dir = rw_dir + 18
            else:
                opp_dir = rw_dir - 18
            
            if len(rw) == 2:
                pos = ''
            elif rw[2] == 'R':
                pos = 'L'
            elif rw[2] == 'L':
                pos = 'R'
            elif rw[2] == 'C':
                pos = 'C'
            
            opp_rw = ("%02d" % opp_dir) + pos
            
            all_runways.append(rw)
            all_runways.append(opp_rw)
        
        return all_runways
    
    
    def get_parking_list(self):
        if self.parking is None and self.fg is not None:
            self.parking = list(self.fg.get_parking_positions(self))
        
        return self.parking
        
    
    def get_starting_positions(self):
        starting_positions = {}
        
        runways = self.get_runway_list()
        parking = self.get_parking_list()
        
        if runways: starting_positions['runway']  = runways
        if parking: starting_positions['parking'] = parking
        
        return starting_positions
    
    
    def has_metar(self):
        return self.metar
    
    def is_installed(self, avaible_scenery):
        if self.get_scenery_tile() in avaible_scenery:
            return True
        else:
            return False
        
    
    def get_closest_metar_airport(self, airportList):
        if self.has_metar():
            return self
        
        min_dist = 100
        next_metar_airport = None
        
        for airport in airportList:
            if airport.metar:
                v = math.fabs(self.coords[0] - airport.coords[0])
                if v > 180: v = 360 - v
                
                h = math.fabs(self.coords[1] - airport.coords[1])
                if h > 180: v = 360 - v
                
                if v*v + h*h < min_dist:
                    min_dist           = v*v + h*h
                    next_metar_airport = airport
        
        return next_metar_airport
    

