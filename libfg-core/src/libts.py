#!/usr/bin/env python2

import libfg_defaults

import os
import subprocess

def removeLockFiles(ts_scenery):
    for f in os.listdir(ts_scenery):
        if f == ".svn":
            if os.path.isfile(ts_scenery + "/.svn/lock"):
                os.remove(ts_scenery + "/.svn/lock")
        elif os.path.isdir(os.path.join(ts_scenery, f)):
            removeLockFiles(os.path.join(ts_scenery, f))



class Terrasync():
    def __init__(self, binary, scenery_dir='', port=libfg_defaults.ts_port):
        self.binary      = binary
        self.scenery_dir = scenery_dir
        self.port        = port
        self.process     = None
    
    def get_binary(self):
        return self.binary
    
    def get_port(self):
        return self.port
    
    def get_scenery_dir(self):
        return self.scenery_dir
    
    
    def start(self):
        command = [ self.binary, '-p', str(self.port), '-S', '-d', self.scenery_dir ]
        self.process = subprocess.Popen(command)
        
        return ' '.join(command)
    
    def stop(self):
        self.process.terminate()
    
    
if __name__ == "__main__":
    removeLockFiles("/home/raphael/.fgfs/terrasync")
    import time
    ts = Terrasync("/usr/bin/terrasync", "/home/raphael/.fgfs/terrasync", 5601)
    command = ts.start()
    print('starting terrasync with following command:')
    print(command)
    time.sleep(10)
    
    ts.stop()
    print('\n')
    